#Variables
#=========
export HISTFILESIZE=20000
export HISTSIZE=10000

PATH="$HOME/Scripts:$PATH"

#Aliases
#=======
#LS Stuff
#enable colored ls
alias ls='ls --color=auto'
alias ll='ls -alh'
alias la='ls -A'
alias l='ls -CFlh'
alias lsd="ls -alF | grep /$"
# This is GOLD for finding out what is taking so much space on your drives!
alias diskspace="du -S | sort -n -r |more"
# Show me the size (sorted) of only the folders in this directory
alias folders="find . -maxdepth 1 -type d -print | xargs du -sk | sort -rn"
# SSH alias because i'm to lazy to type out the full addres
alias csunix="ssh camitzel@cs.dsunix.net"
#Functions
#=========
#Set Longer history
shopt -s histappend
# Combine multiline commands into one in history
shopt -s cmdhist
# Ignore duplicates, ls without options and builtin commands
HISTCONTROL=ignoredups
export HISTIGNORE="&:ls:[bf]g:exit"

# Go up a set number of dirs
up(){
  local d=""
  limit=$1
  for ((i=1 ; i <= limit ; i++))
    do
      d=$d/..
    done
  d=$(echo $d | sed 's/^\///')
  if [ -z "$d" ]; then
    d=..
  fi
  cd $d
}

#set the command prompt
PS1='\u $: '

